package server;



import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        try {
            Socket socket = new Socket("localhost", 8080);
            Scanner scanner = new Scanner(System.in);
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);{
                String response="жду ответа";
                dataOutputStream.writeUTF(response);
                while ((response = dataInputStream.readUTF()) !=null) {
                    System.out.println("Сервер прислал " + response);
                    response = scanner.nextLine();
                    if(response.equals("exit")) {
                        break;
                    }
                    dataOutputStream.writeUTF(response);
                    System.out.println("Отправленно серверу " + response);
                    outputStream.flush();
                }
            }
        } catch (InterruptedIOException e){
            e.printStackTrace();
        }
    }

}
